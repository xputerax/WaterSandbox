document.addEventListener('alpine:init', () => {
    window.Alpine.data('virtualspace', () => ({
        currentPage: "play",
        spaces: [
            {id: '123123', name: 'Test', createdAt: new Date(), },
            {id: 'ada', name: 'ing', createdAt: new Date(), },
            {id: 'adadadad', name: '123', createdAt: new Date(), },
        ],
        availableThemes: [],
        selectedTheme: "",
        selectedFish: "",
        selectedCoral: "",
        theme: {
            name: "",
            background: "",
            // assets available for this theme
            fishes: [],
            corals: [],
        },
        currentSpace: {
            fishes: [],
            corals: [],
        },
        setTheme(theme) {
            this.theme = theme;
            console.log('setting theme to: ')
            console.log(theme)
            this.selectedTheme = theme.name;

//            document.querySelector('#playground').style.background = `url('${theme.background}')`;
        },
        onInit() {
            this.fetchDefault().then(() => {
                this.initSpace()
            })
            this.fetchSpaces()
            this.fetchAvailableThemes()
        },
        async fetchDefault() {
            const endpoint = `/api/default_theme`;
            return fetch(endpoint).then(r => r.json())
                .then(r => this.setTheme(r))
        },
        fetchSpaces() {
            console.log('fetching spaces')
        },
        initSpace() {
            console.log(this.$refs);
            let canvas = this.$refs.canvas;
            let ctx = canvas.getContext("2d");

            var background = new Image();
            background.src = this.theme.background;

            // Make sure the image is loaded first otherwise nothing will draw.
            background.onload = function(){
                ctx.drawImage(background,0,0);
            }
        },
        playAround() {
            if (this.currentPage == 'play') {
                return
            }
            this.currentPage = 'play';
            this.initSpace()
        },
        fetchAvailableThemes() {
            const endpoint = `/api/available_themes`
            fetch(endpoint)
                .then(r => r.json())
                .then(r => {
                    this.availableThemes = r;
                })
        },
        changeTheme(name) {
            const newTheme = this.availableThemes[name]
            console.log('setting new theme')
            console.log(newTheme);
            this.theme = newTheme;
            this.initSpace();
            this.selectedFish = "";
            this.selectedCoral = "";
        },

    }))
})
