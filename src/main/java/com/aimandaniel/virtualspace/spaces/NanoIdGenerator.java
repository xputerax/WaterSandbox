package com.aimandaniel.virtualspace.spaces;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;

import java.util.Random;

public class NanoIdGenerator implements IdGenerator {
    Random rng;
    int size;
    char[] alphabets;

    public NanoIdGenerator(Random rng, int size, char[] alphabets) {
        this.rng = rng;
        this.size = size;
        this.alphabets = alphabets;
    }

    @Override
    public String generate() {
        return NanoIdUtils.randomNanoId(rng, alphabets, size);
    }
}
