package com.aimandaniel.virtualspace.spaces;

import com.aimandaniel.virtualspace.themes.Theme;

public class VirtualSpace {
    Theme theme;

    public VirtualSpace() {
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }
}
