package com.aimandaniel.virtualspace.spaces;

public interface SpaceRepository {
    String save(VirtualSpace vs);
    VirtualSpace fetch(String id);
}
