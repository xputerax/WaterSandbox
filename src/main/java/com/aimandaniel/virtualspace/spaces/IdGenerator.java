package com.aimandaniel.virtualspace.spaces;

public interface IdGenerator {
    String generate();
}
