package com.aimandaniel.virtualspace.spaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisSpaceRepository implements SpaceRepository {
    Logger logger = LoggerFactory.getLogger(RedisSpaceRepository.class);
    private String key;
    private IdGenerator idGenerator;
    private JedisPool pool;
    private Jedis conn;
    private ObjectMapper objectMapper;

    public RedisSpaceRepository(String key, IdGenerator idGenerator, JedisPool pool, ObjectMapper objectMapper) {
        this.key = key;
        this.idGenerator = idGenerator;
        this.pool = pool;
        this.conn = pool.getResource();
        this.objectMapper = objectMapper;
    }

    @Override
    public String save(VirtualSpace space) {
        boolean error = false;
        String id = idGenerator.generate();

        try {
            String serialized = objectMapper.writeValueAsString(space);
            String storeAt = generateHashKey(id);

            conn.set(storeAt, serialized);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e, space);
            error = true;
        }

        if (error) {
            return null;
        }

        return id;
    }

    protected String generateHashKey(String id) {
        return this.key + ":" + id;
    }

    @Override
    public VirtualSpace fetch(String id) {
        VirtualSpace vs = null;
        String fullKey = generateHashKey(id);

        try {
            String serialized = conn.get(fullKey);
            VirtualSpace virtualSpace = objectMapper.readValue(serialized, VirtualSpace.class);
            vs = virtualSpace;
        } catch (JsonMappingException e) {
            logger.error(e.getMessage(), e, id);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e, id);
        }

        return vs;
    }
}
