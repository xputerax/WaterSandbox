package com.aimandaniel.virtualspace.config;

import com.aimandaniel.virtualspace.spaces.IdGenerator;
import com.aimandaniel.virtualspace.spaces.NanoIdGenerator;
import com.aimandaniel.virtualspace.spaces.RedisSpaceRepository;
import com.aimandaniel.virtualspace.spaces.SpaceRepository;
import com.aimandaniel.virtualspace.themes.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import redis.clients.jedis.JedisPool;

import java.util.Arrays;
import java.util.Random;

@Configuration
public class VirtualSpaceConfig {
    private static final String DEFAULT_THEME = "Aquarium";
    private static final String REDIS_THEME_KEY = "virtualspace:themes";
    private static final String REDIS_SPACES_KEY = "virtualspace:spaces";

    @Bean
    @Primary
    public ThemeRegistry themeRegistry() {
        return new InMemoryThemeRegistry();
    }

    @Bean("redisThemeRegistry")
    public ThemeRegistry redisThemeRegistry(JedisPool pool, ObjectMapper objectMapper) {
        return new RedisThemeRegistry(REDIS_THEME_KEY, pool, objectMapper);
    }

    @Bean
    public SpaceRepository spaceRepository(IdGenerator idGenerator, JedisPool pool, ObjectMapper objectMapper) {
        return new RedisSpaceRepository(REDIS_SPACES_KEY, idGenerator, pool, objectMapper);
    }

    @Bean
    public IdGenerator idGenerator() {
        Random random = new Random();
        int size = 10;
        char[] alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();

        return new NanoIdGenerator(random, size, alphabets);
    }

    @Bean("defaultTheme")
    public Theme defaultTheme(ThemeRegistry registry) {
        return new AquariumTheme();
    }

    @Bean
    public ThemeCatalog themeCatalog(
            ThemeRegistry themeRegistry1,
            @Qualifier("redisThemeRegistry")
            ThemeRegistry themeRegistry2
    ) {
        return new ThemeCatalog(Arrays.asList(
                themeRegistry1, themeRegistry2
        ));
    }
}
