package com.aimandaniel.virtualspace.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;

@Configuration
public class JedisConfig {
    @Value("${redis.host}")
    String host;

    @Value("${redis.port}")
    int port;

//    @Value("${redis.connTimeout}")
//    int connTimeout = 10;
//
//    @Value("${redis.soTimeout")
//    int soTimeout = 10;
//
//    @Value("${redis.password}")
//    String password;
//
//    @Value("${redis.database}")
//    String database;
//
//    @Value("${redis.clientName}")
//    String clientName;

    @Bean
    JedisPool jedisPool() {
        JedisPool pool = new JedisPool(host, port);
        return pool;
    }
}
