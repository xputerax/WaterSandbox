package com.aimandaniel.virtualspace.controllers;

import com.aimandaniel.virtualspace.spaces.SpaceRepository;
import com.aimandaniel.virtualspace.spaces.VirtualSpace;
import com.aimandaniel.virtualspace.themes.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TestController {
    JedisPool jedisPool;
    Jedis conn;
    ThemeRegistry themeRegistry;
    ThemeCatalog themeCatalog;
    SpaceRepository spaceRepository;

    public TestController(JedisPool jedisPool, @Qualifier("redisThemeRegistry") ThemeRegistry themeRegistry, ThemeCatalog themeCatalog) {
        this.jedisPool = jedisPool;
        this.conn = jedisPool.getResource();
        this.themeRegistry = themeRegistry;
        this.themeCatalog = themeCatalog;
    }

    @Autowired
    public void setSpaceRepository(SpaceRepository spaceRepository) {
        this.spaceRepository = spaceRepository;
    }

    @GetMapping("/all")
    public List<Theme> all() {
        List<Theme> themes = themeRegistry.getAll();
        return themes;
    }

    @GetMapping("/store")
    public String store() {
        themeRegistry.registerTheme(new AquariumTheme());
        return "OK";
    }

    @GetMapping("/read")
    public List<String> read() {
        return themeCatalog.getNames();
//        List<Theme> themes = themeRegistry.getAll();
//        return themes.stream().map(t -> t.getName()).collect(Collectors.toList());
    }

    @GetMapping("/one")
    public Theme getOne() {
        Theme theme = themeRegistry.getTheme("Aquarium");
        return theme;
    }

    @GetMapping("/save")
    public String save() {
        VirtualSpace vs = new VirtualSpace();
        vs.setTheme(new AquariumTheme());

        String newId = spaceRepository.save(vs);

        return newId;
    }

}
