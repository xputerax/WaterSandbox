package com.aimandaniel.virtualspace.controllers;

import com.aimandaniel.virtualspace.themes.AquariumTheme;
import com.aimandaniel.virtualspace.themes.Theme;
import com.aimandaniel.virtualspace.themes.ThemeCatalog;
import com.aimandaniel.virtualspace.themes.ThemeRegistry;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ApiController {
    ThemeRegistry themeRegistry;

    public ApiController(ThemeRegistry themeRegistry) {
        this.themeRegistry = themeRegistry;
    }

    @GetMapping("/api/default_theme")
    public Map<String, Object> defaultTheme() {
        Theme theme = new AquariumTheme();
        Map<String, Object> ret = new HashMap<>();
        ret.put("name", theme.getName());
        ret.put("background", theme.createBackground());
        ret.put("fishes", theme.getAvailableFishes());
        ret.put("corals", theme.getAvailableCorals());

        return ret;
    }

    @GetMapping("/api/available_themes")
    public Map<String, Object> availableThemes() {
        Map<String, Object> retval = new HashMap<>();

        themeRegistry.getAll().forEach(theme -> {
            Map<Object, Object> data = new HashMap<>();
            data.put("name", theme.getName());
            data.put("background", theme.createBackground());
            data.put("fishes", theme.getAvailableFishes());
            data.put("corals", theme.getAvailableCorals());
            retval.put(theme.getName(), data);
        });

        return retval;
    }

    @GetMapping("/api/catalog")
    public List<String> catalog(ThemeCatalog catalog) {
        return catalog.getNames();
    }
}

class ThemeNotFoundException extends RuntimeException {
    public ThemeNotFoundException(String message) {
        super(message);
    }
}
