package com.aimandaniel.virtualspace.controllers;

import com.aimandaniel.virtualspace.spaces.SpaceRepository;
import com.aimandaniel.virtualspace.themes.ThemeRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class HomeController {
    SpaceRepository spaceRepository;
    ThemeRegistry themeRegistry;

    public HomeController(SpaceRepository spaceRepository, ThemeRegistry themeRegistry) {
        this.spaceRepository = spaceRepository;
        this.themeRegistry = themeRegistry;
    }

    @GetMapping({"/", ""})
    public String homepage() {
        return "home";
    }

    @GetMapping("/space/{id}")
    public String viewSpace(@PathVariable String id) {
        System.out.println("fetching space from repo " + id);
        spaceRepository.fetch(id);
        return "view";
    }
}
