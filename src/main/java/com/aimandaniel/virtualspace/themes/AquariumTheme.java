package com.aimandaniel.virtualspace.themes;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class AquariumTheme implements Theme {
    private String THEME_ASSET_URL_BASE = "/assets/aquarium/";
    private Map<String, String> fishAssets = new HashMap<>();
    private Map<String, String> coralAssets = new HashMap<>();

    public AquariumTheme() {
        fishAssets.put("Green Fish", THEME_ASSET_URL_BASE + "greenfish.png");
        fishAssets.put("Black Fish", THEME_ASSET_URL_BASE + "blackfish.png");
        fishAssets.put("Yellow Fish", THEME_ASSET_URL_BASE + "yellowfish.png");
        fishAssets.put("Orange Fish", THEME_ASSET_URL_BASE + "orangefish.png");
    }

    @Override
    public String getName() {
        return "Aquarium";
    }

    @Override
    public String createBackground() {
        System.out.println("Creating background for aquarium");
        String background = THEME_ASSET_URL_BASE + "aquarium-with-orange-blue-fish.jpg";
        System.out.println(background);
        return background;
    }

    @Override
    public String createFish(String name) {
        String assetUrl = this.fishAssets.get(name);

        if (assetUrl == null) {
            throw new ThemeException("Invalid fish type");
        }

        return "Fish " + name + " asset: " + assetUrl;
    }

    @Override
    public String createCoral(String name) {
        String assetUrl = this.coralAssets.get(name);

        if (assetUrl == null) {
            throw new ThemeException("Invalid coral type");
        }

        return "Coral " + name + " asset: " + assetUrl;
    }

    @Override
    public Map<String, String> getAvailableFishes() {
        return Collections.unmodifiableMap(this.fishAssets);
    }

    @Override
    public Map<String, String> getAvailableCorals() {
        return Collections.unmodifiableMap(this.coralAssets);
    }
}
