package com.aimandaniel.virtualspace.themes;

import java.util.ArrayList;
import java.util.List;

public class ThemeCatalog {
    List<ThemeRegistry> registries;

    public ThemeCatalog(List<ThemeRegistry> registries) {
        this.registries = registries;
    }

    public List<Theme> getAll() {
        List<Theme> ret = new ArrayList<>();

        for (ThemeRegistry reg: registries) {
            ret.addAll(reg.getAll());
        }

        return ret;
    }

    public List<String> getNames() {
        List<String> names = new ArrayList<>();

        for (ThemeRegistry reg: registries) {
            names.addAll(reg.getNames());
        }

        return names;
    }

    public Theme getTheme(String name) {
        for (ThemeRegistry reg: registries) {
            Theme theme = reg.getTheme(name);
            if (theme != null) {
                return theme;
            }
        }

        return null;
    }
}
