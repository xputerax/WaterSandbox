package com.aimandaniel.virtualspace.themes;

import java.util.List;
import java.util.Map;

public interface Theme {
    String getName();
    String createBackground();
    String createFish(String name);
    String createCoral(String name);
    Map<String, String> getAvailableFishes();
    Map<String, String> getAvailableCorals();

}
