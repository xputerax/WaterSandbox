package com.aimandaniel.virtualspace.themes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;
import java.util.stream.Collectors;

public class RedisThemeRegistry implements ThemeRegistry {
    Logger logger = LoggerFactory.getLogger(RedisThemeRegistry.class);
    JedisPool pool;
    Jedis conn;
    ObjectMapper objectMapper;
    String key;

    public RedisThemeRegistry(String key, JedisPool pool, ObjectMapper objectMapper) {
        this.pool = pool;
        this.conn = pool.getResource();
        this.objectMapper = objectMapper;
        this.key = key;
    }

    @Override
    public void registerTheme(Theme t) {
        logger.debug("registering theme " + t.getName());
        try {
            String serialized = objectMapper.writeValueAsString(t);
            conn.hsetnx(key, t.getName(), serialized);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public Theme getTheme(String name) {
        logger.debug("getting theme " + name);

        Theme theme = null;
        String serialized = conn.hget(key, name);

        try {
            JsonNode json = objectMapper.readTree(serialized);
            String themeType = json.get("name").asText();

            if (themeType.equals("Aquarium")) {
                AquariumTheme temp = new AquariumTheme();
                theme = temp;
            } else if (themeType.equals("Deep Ocean")) {
                DeepOceanTheme temp = new DeepOceanTheme();
                theme = temp;
            } else {
                // TODO: invalid type
            }
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e, name, key);
        }

        System.out.println(serialized);
        return theme;
    }

    @Override
    public List<String> getNames() {
        logger.debug("getting all names ");

        return conn.hkeys(key).stream().toList();
    }

    @Override
    public List<Theme> getAll() {
        List<Theme> themes = getNames().stream().map(name -> {
            Theme theme = getTheme(name);
            return theme;
        }).collect(Collectors.toList());

        return themes;
    }
}
