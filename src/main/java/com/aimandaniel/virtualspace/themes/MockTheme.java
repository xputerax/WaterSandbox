package com.aimandaniel.virtualspace.themes;

import java.util.Map;

public class MockTheme implements Theme {
    @Override
    public String getName() {
        return "mock theme";
    }

    @Override
    public String createBackground() {
        return "bg.jpeg";
    }

    @Override
    public String createFish(String name) {
        return null;
    }

    @Override
    public String createCoral(String name) {
        return null;
    }

    @Override
    public Map<String, String> getAvailableFishes() {
        return null;
    }

    @Override
    public Map<String, String> getAvailableCorals() {
        return null;
    }
}
