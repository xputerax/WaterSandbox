package com.aimandaniel.virtualspace.themes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemoryThemeRegistry implements ThemeRegistry {
    private Map<String, Theme> themes = new HashMap<>();

    @Override
    public void registerTheme(Theme t) {
        themes.put(t.getName(), t);
    }

    @Override
    public Theme getTheme(String name) {
        return themes.get(name);
    }

    @Override
    public List<String> getNames() {
        return themes.entrySet().stream()
                .map((t) -> t.getKey())
                .collect(Collectors.toList());
    }

    @Override
    public List<Theme> getAll() {
        return this.themes.entrySet().stream()
                .map(entry -> entry.getValue())
                .collect(Collectors.toList());
    }


}
