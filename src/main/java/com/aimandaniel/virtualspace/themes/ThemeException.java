package com.aimandaniel.virtualspace.themes;

public class ThemeException extends RuntimeException {
    public ThemeException(String message) {
        super(message);
    }
}
