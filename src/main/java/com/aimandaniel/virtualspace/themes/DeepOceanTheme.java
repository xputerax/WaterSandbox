package com.aimandaniel.virtualspace.themes;

import java.util.HashMap;
import java.util.Map;

public class DeepOceanTheme implements Theme {
    private String ASSET_BASE = "/assets/deep_water/";
    private String FISH_ASSET = ASSET_BASE + "coloured-fishes-collection/";
    private Map<String, String> fishes = new HashMap<>();
    private Map<String, String> corals = new HashMap<>();

    public DeepOceanTheme() {
        this.fishes.put("Grouper", FISH_ASSET + "grouper.png");
        this.fishes.put("Snapper", FISH_ASSET + "snapper.png");
        this.fishes.put("Angry Fish", FISH_ASSET + "angry_fish.png");
        this.fishes.put("Green Fish", FISH_ASSET + "green_fish.png");
        this.fishes.put("Huge Tuna", FISH_ASSET + "tuna.png");
    }

    @Override
    public String getName() {
        return "Deep Ocean";
    }

    @Override
    public String createBackground() {
        return ASSET_BASE + "backgrounds/underwater-world-with-fish-corals-generative-ai.jpg";
    }

    @Override
    public String createFish(String name) {
        return "creating deep ocean fish " + name;
    }

    @Override
    public String createCoral(String name) {
        return "creating deep ocean coral " + name;
    }

    @Override
    public Map<String, String> getAvailableFishes() {
        return fishes;
    }

    @Override
    public Map<String, String> getAvailableCorals() {
        return corals;
    }
}
