package com.aimandaniel.virtualspace.themes;

import java.util.Iterator;
import java.util.List;

public interface ThemeRegistry {
    void registerTheme(Theme t);
    Theme getTheme(String name);
    List<String> getNames();
    List<Theme> getAll();
}
