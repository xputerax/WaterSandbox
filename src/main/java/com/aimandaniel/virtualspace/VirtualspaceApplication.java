package com.aimandaniel.virtualspace;

import com.aimandaniel.virtualspace.themes.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class VirtualspaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(VirtualspaceApplication.class, args);
	}

	@Bean
	public CommandLineRunner loadThemesIntoRegistry(ThemeRegistry themeRegistry) {
		return (args) -> {
			themeRegistry.registerTheme(new AquariumTheme());
			themeRegistry.registerTheme(new DeepOceanTheme());
		};
	}

}
